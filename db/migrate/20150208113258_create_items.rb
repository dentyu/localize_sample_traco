class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name_en, :name_ja

      t.timestamps
    end
  end
end
