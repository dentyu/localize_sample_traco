class SettingsController < ApplicationController

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    redirect_to :root
  end

end
