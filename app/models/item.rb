class Item < ActiveRecord::Base
  translates :name

  validates *locale_columns(:name), :uniqueness => true

  def as_json(options={})
    super(
      options.merge({
        only: [:id],
        methods: [:name],
      })
    )
  end
end
